#include "databasecalls.h"
#include <QDebug>
#include <QSqlQuery>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QSqlError>
#include <QMessageBox>
#include <QFile>
#include <QDate>

//------------------------------------------------------------------------------
DataBaseCalls::DataBaseCalls(const QString &filedb, QObject *parent) :
    QObject(parent)
{
    dbName = filedb;
    tableCalls = "calls";
}

//------------------------------------------------------------------------------
DataBaseCalls::~DataBaseCalls()
{
}

//------------------------------------------------------------------------------
void DataBaseCalls::createTable()
{
    if (!createConnection()) {
        return;
    }

    QString sqlCreateTable = QString(
                "CREATE TABLE IF NOT EXISTS %1 ( "
                "datetime INTEGER not null, "
                "duration INTEGER not null, "
                "seg      TEXT null, "
                "sop      TEXT null, "
                "dest     TEXT null, "
                "dirIn    TEXT null, "
                "dirOut   TEXT null, "
                "str1     TEXT null, "
                "str2     TEXT null "
                ");"
                ).arg(tableCalls);

    QSqlQuery query;
    if (!query.exec(sqlCreateTable)) {
        qDebug() << "Unable to create a table";
        return;
    }
}

//------------------------------------------------------------------------------
bool DataBaseCalls::insertValues(const QStringList &listValues, const QString &sqlInsert)
{
    QString str = sqlInsert;
    foreach (QString var, listValues) {
        str = str.arg(var);
    }

    QSqlQuery query;
    if (!query.exec(str)) {
        qDebug() << "Unable to make insert operation:" << str;
        return false;
    }

    return true;
}

//------------------------------------------------------------------------------
void DataBaseCalls::slotFillTable(const QString &fileName)
{
    QString sqlInsert = QString(
                "INSERT INTO %1 (datetime, duration, seg, sop, "
                "dest, dirIn, dirOut, str1, str2) "
                "VALUES('%2', '%3', '%4', %5, '%6', '%7', '%8', '%9', %10)"
                ).arg(tableCalls);


    QFile currentFile(fileName);
    if (!currentFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Error opening file:" << fileName;
        return;
    }

    QString year = QDate::currentDate().toString("yyyy");
    QRegExp rxDateTime;
    rxDateTime.setPattern("(\\d{2})(\\d{2})(\\d{2})(\\d{2})");
    QRegExp rxDuration;
    rxDuration.setPattern("(\\d{1})(\\d{2})(\\d{2})");

    QSqlDatabase db = QSqlDatabase::database();
    db.transaction();
    while (!currentFile.atEnd()) {
        QString line = currentFile.readLine();
        line.replace("\n", "");
        QStringList lst = line.split(" ", QString::SkipEmptyParts);

        if (lst.size() < 6) {
            continue;
        }
        if (rxDateTime.indexIn(lst[0]) != -1) {
            lst[0] = year + "-" + rxDateTime.cap(1) + "-" + rxDateTime.cap(2)
                     + " " + rxDateTime.cap(3) + ":" + rxDateTime.cap(4);
        }
        if (rxDuration.indexIn(lst[1]) != -1) {
            lst[1] = "0" + rxDuration.cap(1) + ":" + rxDuration.cap(2) + ":"
                     + rxDuration.cap(3);
        }
        if (!lst[2].contains(QRegExp("[A-Za-z]"))) {
            lst.insert(2, "");
        }
        if(lst.size() != 9) {
            lst.insert(3, "NULL");
        }
        if(lst.size() != 9) {
            lst.insert(4, "");
        }
        if (!insertValues(lst, sqlInsert)) {
            break;
        }
    }

    if (!db.commit()) {
        qDebug() << "Error: transaction refuse";
        db.rollback();
    }

    currentFile.close();
    currentFile.remove();

    QStringList listTables = db.tables(QSql::Tables);
    QStringList listViews = db.tables(QSql::Views);
    emit signalConnectDataBase(listTables + listViews);
    emit signalUpdateTable();
}

//------------------------------------------------------------------------------
bool DataBaseCalls::createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbName);

    if (!db.open()) {
        qDebug() << "Cannot open database " << db.lastError().text();
        return false;
    }

    QStringList listTables = db.tables(QSql::Tables);
    QStringList listViews = db.tables(QSql::Views);
    emit signalConnectDataBase(listTables + listViews);

    return true;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
