#-------------------------------------------------
#
# Project created by QtCreator 2014-09-08T13:32:26
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = callsBase
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    databasecalls.cpp

HEADERS  += mainwindow.h \
    databasecalls.h

QMAKE_CXXFLAGS += -std=c++11 -Wall

RC_FILE = icon.rc

RESOURCES += \
    images.qrc
