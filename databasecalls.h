#ifndef DATABASECALLS_H
#define DATABASECALLS_H

#include <QObject>
//#include <QSqlQuery>

class DataBaseCalls : public QObject
{
    Q_OBJECT
public:
    explicit DataBaseCalls(const QString &filedb, QObject *parent = 0);
    ~DataBaseCalls();
    void createTable();

signals:
    void signalConnectDataBase(const QStringList &listTables);
    void signalUpdateTable();

public slots:
    void slotFillTable(const QString &fileName);

private:
    bool createConnection();
    bool insertValues(const QStringList &listValues, const QString &sqlInsert);

private:
    QString dbName;
    QString tableCalls;
//    QSqlQuery query;

};

#endif // DATABASECALLS_H
