#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "databasecalls.h"

class QTableView;
class QDateTimeEdit;
class QLineEdit;
class QAction;
class QSqlQueryModel;
class QLabel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void signalDataBaseUpdate(const QString &fileName);

private slots:
    void slotSetLogFile();
    void slotDataBaseUpdate();
    void slotReceiveListTables(const QStringList &tables);
    void slotShowTables();
    void slotSaveToFile();

private:
    void createWidgets();
    void createActions();
    void createConnect();
    void createWorkDirectory();
    void showRowsCount(QString strQuery);
    QString copyLogFile(QString dirName, QString fileName) const;
    QString endSymbol(int column);
    QString printLine(int length);

private:
    QTableView    *tableView;
    QDateTimeEdit *firstDateTime;
    QDateTimeEdit *lastDateTime;
    QLineEdit     *inNumber;
    QLineEdit     *outNumber;
    QLineEdit     *allNumber;
    QAction       *actionUpdate;
    QAction       *actionPathToLogfile;
    QAction       *actionSaveToFile;
    DataBaseCalls *dbase;
    QString       logFileName;
    QString       baseDir;
    QStringList   dbTables;
    QSqlQueryModel *qmodel;
    QLabel        *labelRowsCount;

};

#endif // MAINWINDOW_H
