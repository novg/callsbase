#include "mainwindow.h"
#include <QDebug>
#include <QLabel>
#include <QTableView>
#include <QDateTimeEdit>
#include <QLineEdit>
#include <QBoxLayout>
#include <QAction>
#include <QToolBar>
#include <QFileDialog>
#include <QMessageBox>
#include <QValidator>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSqlError>
#include <QHeaderView>

//------------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    createWidgets();
    createActions();

    const QString baseName = "calls.db";
    baseDir  = "dbase";
    createWorkDirectory();

    dbase    = new DataBaseCalls(baseDir + "/" + baseName);

    createConnect();
    dbase->createTable();
}

//------------------------------------------------------------------------------
MainWindow::~MainWindow()
{
    delete dbase;
}

//------------------------------------------------------------------------------
void MainWindow::slotSetLogFile()
{
    logFileName = QFileDialog::getOpenFileName(this, tr("Open logfile"), "",
                   tr("Log files (*.log);;All files (*)"));

    if (logFileName != "") {
        actionUpdate->setEnabled(true);
    }
}

//------------------------------------------------------------------------------
void MainWindow::slotDataBaseUpdate()
{
    if (logFileName != "") {
        QString fileName = copyLogFile(baseDir, logFileName);
        emit signalDataBaseUpdate(fileName);
    }
}

//------------------------------------------------------------------------------
void MainWindow::slotReceiveListTables(const QStringList &tables)
{
    dbTables = tables;
}

//------------------------------------------------------------------------------
void MainWindow::slotShowTables()
{
    if (dbTables.isEmpty()) {
        return;
    }

    qmodel = new QSqlQueryModel(this);
    QString strDirIn  = inNumber->text();
    QString strDirOut = outNumber->text();
    QString strFirstDateTime =
            firstDateTime->dateTime().toString("yyyy-MM-dd hh:mm");
    QString strLastDateTime =
            lastDateTime->dateTime().toString("yyyy-MM-dd hh:mm");
    QString strQuery;
    QString strRowsCount;

    if (strDirIn == "" and strDirOut == "") {
        strQuery = QString("SELECT datetime, duration, dirIn, dirOut FROM %1 "
                           "WHERE datetime "
                           "BETWEEN datetime('%2') AND datetime('%3') "
                           )
                .arg(dbTables[0])
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                ;
        strRowsCount = QString("SELECT COUNT(*) FROM %1 "
                           "WHERE datetime "
                           "BETWEEN datetime('%2') AND datetime('%3') "
                           )
                .arg(dbTables[0])
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                ;
    } else {
        strQuery = QString("SELECT datetime, duration, dirIn, dirOut FROM %1 "
                           "WHERE datetime "
                           "BETWEEN datetime('%2') AND datetime('%3') "
                           "AND (dirIn IN (%4) OR dirOut IN (%5))"
                           )
                .arg(dbTables[0])
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirIn)
                .arg(strDirOut)
                ;
        strRowsCount = QString("SELECT COUNT(*) FROM %1 "
                           "WHERE datetime "
                           "BETWEEN datetime('%2') AND datetime('%3') "
                           "AND (dirIn IN (%4) OR dirOut IN (%5))"
                           )
                .arg(dbTables[0])
                .arg(strFirstDateTime)
                .arg(strLastDateTime)
                .arg(strDirIn)
                .arg(strDirOut)
                ;

    }

    showRowsCount(strRowsCount);
    qmodel->setQuery(strQuery);

    if (qmodel->lastError().isValid()) {
        qDebug() << qmodel->lastError().text();
    }

    tableView->setModel(qmodel);
    tableView->resizeColumnsToContents();
    tableView->resizeRowsToContents();
}

//------------------------------------------------------------------------------
void MainWindow::slotSaveToFile()
{
    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save to file"),
                                 "", tr("Text files (*.txt);;All files (*)"));

    if (saveFileName != "") {
        QFile saveFile(saveFileName);
        if (saveFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream stream(&saveFile);
            int length = 67;
            stream << printLine(length);
            stream << tr("Дата\t   Время") << "\t"
                   << tr("Продолжит.")     << "\t"
                   << tr("Входящий")       << "\t"
                   << tr("Исходящий")      << "\n"
                      ;
            stream << printLine(length);

            while (qmodel->canFetchMore()) {
                qmodel->fetchMore();
            }

            int rowsCount   = tableView->verticalHeader()->count();
            int columnCount = tableView->horizontalHeader()->count();

            for (int row = 0; row < rowsCount; ++row) {
                for (int col = 0; col < columnCount; ++col) {
                    QModelIndex index = qmodel->index(row, col, QModelIndex());
                    stream << qmodel->data(index).toString() << endSymbol(col);
                }
            }

            stream << printLine(length);
            saveFile.close();
        }
    }
}

//------------------------------------------------------------------------------
QString MainWindow::printLine(int length)
{
    QString line;
    for (int i = 0; i < length; ++i) {
        line += "-";
    }
    line += "\n";
    return line;
}

//------------------------------------------------------------------------------
QString MainWindow::endSymbol(int column)
{
    switch (column) {
    case 0:
        return "\t";
        break;
    case 1:
        return "\t";
        break;
    case 2:
        return "    \t";
        break;
    default:
        return "\n";
        break;
    }
}

//------------------------------------------------------------------------------
void MainWindow::createWidgets()
{
   QWidget *widgetPanel     = new QWidget;
   QLabel  *labelFirstDate  = new QLabel(tr("Начало периода:"));
   QLabel  *labelLastDate   = new QLabel(tr("Конец периода:"));
   QLabel  *labelInNumber   = new QLabel(tr("Входящие номера:"));
   QLabel  *labelOutNumber  = new QLabel(tr("Исходящие номера:"));
   QWidget *widgetCentral   = new QWidget;
   labelRowsCount = new QLabel(tr("Всего соединений: "));

   tableView     = new QTableView;
   firstDateTime = new QDateTimeEdit(QDateTime(QDate::currentDate(), QTime(0, 0)));
   firstDateTime->setDisplayFormat("yyyy-MM-dd hh:mm");
   lastDateTime  = new QDateTimeEdit(QDateTime::currentDateTime());
   lastDateTime->setDisplayFormat("yyyy-MM-dd hh:mm");
   inNumber      = new QLineEdit;
   outNumber     = new QLineEdit;

   QValidator *validator = new QRegExpValidator(QRegExp("[0-9,]*"));
   inNumber->setValidator(validator);
   outNumber->setValidator(validator);

   // Layout setup
   QVBoxLayout *inNumberLayout  = new QVBoxLayout;
   inNumberLayout->addWidget(labelInNumber);
   inNumberLayout->addWidget(inNumber);

   QVBoxLayout *outNumberLayout = new QVBoxLayout;
   outNumberLayout->addWidget(labelOutNumber);
   outNumberLayout->addWidget(outNumber);

   QVBoxLayout *firstDateLayout = new QVBoxLayout;
   firstDateLayout->addWidget(labelFirstDate);
   firstDateLayout->addWidget(firstDateTime);

   QVBoxLayout *lastDateLayout  = new QVBoxLayout;
   lastDateLayout->addWidget(labelLastDate);
   lastDateLayout->addWidget(lastDateTime);

   QHBoxLayout *groupLayout = new QHBoxLayout;
   groupLayout->addLayout(firstDateLayout);
   groupLayout->addLayout(lastDateLayout);
   groupLayout->addStretch(1);
   groupLayout->addLayout(inNumberLayout);
   groupLayout->addLayout(outNumberLayout);
   widgetPanel->setLayout(groupLayout);

   QVBoxLayout *commonLayout    = new QVBoxLayout;
   commonLayout->addWidget(widgetPanel);
   commonLayout->addWidget(tableView);
   commonLayout->addWidget(labelRowsCount);
   widgetCentral->setLayout(commonLayout);

   setCentralWidget(widgetCentral);
}


//------------------------------------------------------------------------------
void MainWindow::createActions()
{
    actionUpdate        = new QAction(tr("Update the database"), this);
    actionUpdate->setShortcut(QKeySequence(tr("Ctrl+R")));
    actionUpdate->setToolTip(tr("Update the database"));
    actionUpdate->setStatusTip(tr("Update the database"));
    actionUpdate->setWhatsThis(tr("Update the database"));
    actionUpdate->setIcon(QIcon(":/images/db_update.png"));

    if (logFileName == "") {
        actionUpdate->setDisabled(true);
    }

    actionPathToLogfile = new QAction(tr("Path to logfile..."), this);
    actionPathToLogfile->setShortcut(QKeySequence(tr("Ctrl+O")));
    actionPathToLogfile->setToolTip(tr("Path to logfile..."));
    actionPathToLogfile->setStatusTip(tr("Path to logfile..."));
    actionPathToLogfile->setWhatsThis(tr("Path to logfile..."));
    actionPathToLogfile->setIcon(QIcon(":/images/file_download.png"));

    actionSaveToFile = new QAction(tr("Save to file"), this);
    actionSaveToFile->setShortcut(QKeySequence(tr("Ctrl+S")));
    actionSaveToFile->setToolTip(tr("Save to file"));
    actionSaveToFile->setStatusTip(tr("Save to file"));
    actionSaveToFile->setWhatsThis(tr("Save to file"));
    actionSaveToFile->setIcon(QIcon(":/images/save.png"));

    QList<QAction *> actions;
    actions << actionUpdate << actionPathToLogfile << actionSaveToFile;

    QToolBar *toolBar;
    toolBar = addToolBar(tr("CallsBase toolbar"));
    toolBar->addActions(actions);
}

//------------------------------------------------------------------------------
QString MainWindow::copyLogFile(QString dirName, QString fileName) const
{
    QFile sourceFile(fileName);
    QFileInfo infoFile(fileName);
    QString targetFileName = dirName + "/" + infoFile.fileName();

    QFile targetFile(targetFileName);
    if (targetFile.exists()) {
        targetFile.remove();
    }

    if (!sourceFile.copy(targetFileName)) {
        QMessageBox::warning(0, tr("Warning"), tr("Cannot copy file: %1")
                             .arg(fileName));
    }

    return targetFileName;
}

//------------------------------------------------------------------------------
void MainWindow::createWorkDirectory()
{
    QDir dbDir(baseDir);
    if (!dbDir.exists()) {
        QDir::current().mkdir(baseDir);
    }
}

//------------------------------------------------------------------------------
void MainWindow::showRowsCount(QString strQuery)
{
    QSqlQuery sqlQuery(strQuery);
    sqlQuery.first();
    labelRowsCount->setText(tr("Всего соединений: %1")
                            .arg(sqlQuery.value(0).toInt()));
}

//------------------------------------------------------------------------------
void MainWindow::createConnect()
{
    connect(actionPathToLogfile, SIGNAL(triggered()),
            this, SLOT(slotSetLogFile()));
    connect(actionUpdate, SIGNAL(triggered()),
            this, SLOT(slotDataBaseUpdate()));
    connect(dbase, SIGNAL(signalConnectDataBase(QStringList)),
            this, SLOT(slotReceiveListTables(QStringList)));
    connect(firstDateTime, SIGNAL(editingFinished()),
            this, SLOT(slotShowTables()));
    connect(lastDateTime, SIGNAL(editingFinished()),
            this, SLOT(slotShowTables()));
    connect(inNumber, SIGNAL(returnPressed()),
            this, SLOT(slotShowTables()));
    connect(outNumber, SIGNAL(returnPressed()),
            this, SLOT(slotShowTables()));
    connect(this, SIGNAL(signalDataBaseUpdate(QString)),
            dbase, SLOT(slotFillTable(QString)));
    connect(dbase, SIGNAL(signalUpdateTable()),
            this, SLOT(slotShowTables()));
    connect(actionSaveToFile, SIGNAL(triggered()),
            this, SLOT(slotSaveToFile()));
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
